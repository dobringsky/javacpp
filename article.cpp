Костыли и велосипеды — моё любимое хобби. В свободное от работы и хобби время я поклоняюсь иконам Java. Но, к сожалению, на работе приходится писать на ненавистном мне С++. Поэтому я решил написать свою жабу. На плюсах. Что из этого получилось — под катом.

Пакеты можно заменить пространствами имён. 
<@code
namespace java 
{
	namespace lang 
	{
		//...
	}
} 
@code>

В первую очередь нужно избавиться от голых С-указателей. Это просто — на Java они называются ссылками. Вот и мы будем называть их ссылками:
<@code
namespace java 
{
	template <typename _Tp> 
	using ReferenceTo = _Tp*;

	using Reference = ReferenceTo<void>;
}
@code>

Теперь добавим примитивные типы из java:
<@code
using boolean = bool;
using byte = unsigned char;
// using short  	= short int;
// using int 		= int;
// using long 		= long int;
// using float 		= float;
// using double 	= double;
// using char 		= char;
// using void 		= void;
@code>

Вроде бы все. Теперь о классах. Главный класс - java.lang.Object. С него и начнем!

