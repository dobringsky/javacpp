#pragma once

#include <java/lang.hpp>

class java::lang::Object::Impl
{
public:
	Impl() = default;
	virtual ~Impl() = default;

	Impl(const Impl&) = default;
	Impl(Impl&&) noexcept = default;

	Impl& operator=(const Impl&) = default;
	Impl& operator=(Impl&&) noexcept = default;

	virtual boolean equals(const Impl& other) const;
	virtual Impl* clone() const;
	virtual java::lang::Class getClass() const;
	virtual long hashCode() const;
	virtual void finalize();

	template <typename T>
	const T& as() const
	{
		if (const T* other = dynamic_cast<const T*>(this))
		{
			return *other;
		} else throw std::runtime_error("type error");
	}

	// java::Reference operator new(std::size_t);
	// void operator delete(java::Reference);
};