#include <java/lang.hpp>
#include <java/lang/object/impl.hpp>
#include <java/common/java_allocator.hpp>

void 	
java::lang::Object::Impl::finalize()
{

}

java::lang::Class
java::lang::Object::Impl::getClass() const
{
	return java::lang::Class::from<java::lang::Object>();
}

boolean 
java::lang::Object::Impl::equals(const java::lang::Object::Impl& other) const
{
	return (this == &other);
}

java::lang::Object::Impl* 
java::lang::Object::Impl::clone() const
{
	return new Impl(*this);
}

long 	
java::lang::Object::Impl::hashCode() const 
{ 
	return reinterpret_cast<long>(this); 
}

// java::Reference
// java::lang::Object::Impl::operator new(std::size_t n)
// {
// 	return java::Memory::basic().do_alloc(n);
// }

// void 
// java::lang::Object::Impl::operator delete(java::Reference ref)
// {
// 	java::Memory::basic().do_free(ref);
// }