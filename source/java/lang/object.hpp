#pragma once

#include <java.hpp>

namespace java
{
	namespace lang
	{
		/// @Class java.lang.Object
		//! Java top hierarchy super class
		class Object 
		{
		public:

			Object();
			virtual ~Object();

			Object(const Object& other): pimpl(other.pimpl) {}
			Object(Object&& other): pimpl(std::move(other.pimpl)) {}

			Object& operator=(const Object& other) { assign(other); return(*this); }
			Object& operator=(Object&& other) { assign(std::move(other)); return(*this); }

			virtual Object clone() const;
			virtual boolean isNull() const;
			virtual boolean equals(const Object& other) const;
			virtual long hashCode() const;
			virtual java::lang::Class getClass() const; 
			virtual java::lang::String toString() const;
			virtual void finalize();

			template <typename T> 
			T as() const { return T(pimpl); }
			
			/// These methods are deprecated since java::util::concurent released
			// void wait();
			// void notify();
			// void notifyAll();

		/// And some C++ implementation things D:
			// java::Reference operator new(std::size_t);
			// void operator delete(java::Reference);
			static constexpr const char* TYPENAME = "java::lang::Object";
		protected:
			class Impl;
			
			Object(Impl* impl): pimpl(impl) {} 
			Object(const std::shared_ptr<Impl>& impl): pimpl(impl) {}
			Object(std::shared_ptr<Impl>&& impl): pimpl(std::move(impl)) {}

			std::shared_ptr<Impl> pimpl;

			virtual void assign(const Object& o)
			{
				pimpl = o.pimpl;
			}
			virtual void assign(Object&& o)
			{
				pimpl = std::move(o.pimpl);
			}
		private:
			inline Impl& getImpl();
			inline const Impl& getImpl() const;
		};


	}
}
