#include <java.hpp>
#include <java/lang.hpp>
#include <java/primitive/number.hpp>
#include <java/primitive/number/impl.hpp>


java::lang::String 
java::lang::Number::Impl::toDecimalString() const
{
	throw std::runtime_error("Not implemented");
}

java::lang::String 
java::lang::Number::Impl::toBinaryString() const
{
	throw std::runtime_error("Not implemented");
}

java::lang::String 
java::lang::Number::Impl::toOctalString() const
{
	throw std::runtime_error("Not implemented");
}

java::lang::String 
java::lang::Number::Impl::toHexString() const
{
	throw std::runtime_error("Not implemented");
}

int 
java::lang::Number::Impl::compareTo(const java::lang::Number::Impl&) const
{
	throw std::runtime_error("Not implemented");
}