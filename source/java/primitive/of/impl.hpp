#pragma once

#include <java/primitive/number.hpp>
#include <java/primitive/number/impl.hpp>
#include <java/primitive/primitive_of.hpp>

namespace java
{
	namespace detail
	{
		template <typename T>
		struct NumberParser
		{
			static T from(const java::lang::String&)
			{
				throw std::runtime_error("unsupported parser type");
			}
		};
	} /// ns detail
} /// ns java


template <typename T, template <typename> class TCHK>
class java::lang::PrimitiveOf<T, TCHK>::Impl : public java::lang::Number::Impl
{
	using value_type = T;
	using reference = value_type &;
	using pointer = value_type *;
	using const_reference = const value_type &;
	using const_pointer = const value_type *;

	using primitive_type = PrimitiveOf<value_type, TCHK>;

	java_object_impl(primitive_type)
public:
	Impl(): data(0) {}
	~Impl() override = default;

	Impl(const value_type& value): data(value) {}
	Impl(value_type&& value) noexcept: data(std::move(value)) {}

	Impl& operator=(const value_type& value);
	Impl& operator=(value_type&& value) noexcept;

	static java::lang::PrimitiveOf<T, TCHK> fromString(const java::lang::String& inp);

 	java::lang::String toDecimalString() const override;
	java::lang::String toBinaryString() const override;
	java::lang::String toOctalString() const override;
	java::lang::String toHexString() const override;
	int compareTo(const java::lang::Number::Impl&) const override;

	value_type 		get_value() const 		{ return data; }
	reference 		get_ref() 				{ return data; }
	pointer 		get_ptr() 				{ return & data; }
	const_reference get_const_ref() const 	{ return data; }
	const_pointer 	get_const_ptr() const 	{ return & data; }

private:
	value_type data;
};


template <>
struct java::detail::NumberParser<short>
{
	static short from(const java::lang::String& inp)
	{
		return short(std::stoi(inp));
	}
};

template <>
struct java::detail::NumberParser<int>
{
	static int from(const java::lang::String& inp)
	{
		return int(std::stoi(inp));
	}
};

template <>
struct java::detail::NumberParser<long>
{
	static long from(const java::lang::String& inp)
	{
		return long(std::stol(inp));
	}
};

template <>
struct java::detail::NumberParser<float>
{
	static float from(const java::lang::String& inp)
	{
		return float(std::stof(inp));
	}
};

template <>
struct java::detail::NumberParser<double>
{
	static double from(const java::lang::String& inp)
	{
		return double(std::stod(inp));
	}
};

#include <cstring>
template <>
struct java::detail::NumberParser<boolean>
{
	static boolean from(const java::lang::String& inp)
	{
		if (inp[0] == '0' || !strncmp(inp.c_str(), "true", 4) || !strncmp(inp.c_str(), "false", 5)) {
			return inp[0] == '0' || inp[0] == 't';
		} else {
			return boolean(std::stof(inp));
		}
	}
};
