#pragma once

#include <java.hpp>
#include <java/lang.hpp>

class java::lang::Number : public java::lang::Object
{
	java_object(java::lang, Number)
public:
	Number();
	virtual ~Number() = default;
	
	virtual java::lang::String toDecimalString() const;
	virtual java::lang::String toBinaryString() const;
	virtual java::lang::String toOctalString() const;
	virtual java::lang::String toHexString() const;
	virtual int compareTo(const java::lang::Number&) const;
};