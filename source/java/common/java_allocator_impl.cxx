#include <java.hpp>
#include <cstring>

constexpr auto setArraySize = [](java::ReferenceTo<byte>& mem, std::size_t size)
{
	new (mem) std::size_t(size);
	mem += sizeof(std::size_t);
};

constexpr auto getArraySize = [](java::ReferenceTo<byte> mem)
{
	return std::size_t(*(reinterpret_cast<std::size_t*>(mem)-1));
};


template <std::size_t _MemSize>
java::Reference 
java::Allocator_<_MemSize>::do_alloc(std::size_t n)
{
	if (current + n >= end())
	{
		throw std::bad_alloc();
	}
	auto* tmp = current;
	current += n;
	return tmp;
}

template <std::size_t _MemSize>
void 
java::Allocator_<_MemSize>::do_free(java::Reference ref)
{
	memset(ref, 0, 1);
}

template <std::size_t _MemSize>
java::ReferenceTo<byte> 
java::Allocator_<_MemSize>::begin()
{
	return &memory[0];
}

template <std::size_t _MemSize>
java::ReferenceTo<byte> 
java::Allocator_<_MemSize>::end()
{
	return &memory[memsize()];
}

template <std::size_t _MemSize>
template <typename _Tp, typename... _Args>
java::ReferenceTo<_Tp> 
java::Allocator_<_MemSize>::construct(_Args&&... arguments)
{
	if (current + sizeof(_Tp) >= end())
	{
		throw std::bad_alloc();
	}
	auto* tmp = new(current) _Tp(std::forward<_Args>(arguments)...);
	current += sizeof(_Tp);
	return tmp;
}

template <std::size_t _MemSize>
template <typename _Tp>
void 
java::Allocator_<_MemSize>::destroy(java::ReferenceTo<_Tp> garbage)
{
	garbage->~_Tp();
}

template <std::size_t _MemSize>
template <typename _Tp>
java::ReferenceTo<_Tp> 
java::Allocator_<_MemSize>::constructArray(std::size_t arraySize)
{
	if (current + sizeof(_Tp)*arraySize + sizeof(std::size_t) >= end())
	{
		throw std::bad_alloc();
	}
	setArraySize(current, arraySize);
	auto* tmp = new(current) _Tp[arraySize];
	current += sizeof(_Tp)*arraySize;
	return tmp;
}

template <std::size_t _MemSize>
template <typename _Tp>
void 
java::Allocator_<_MemSize>::destroyArray(java::ReferenceTo<_Tp> garbage)
{
	std::size_t arraySize = getArraySize(garbage);
	for (std::size_t idx = 0; idx < arraySize; ++idx)
	{
		garbage[idx].~_Tp();
	}
}