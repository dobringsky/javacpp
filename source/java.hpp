#pragma once

#include <cstdio>
#include <cstdlib>
#include <cstdint>

#include <string>
#include <vector>
#include <unordered_map>
#include <stdexcept>
#include <memory>
#include <utility>
#include <array>

#include <javafwd.hpp>

namespace java
{

	namespace lang 
	{
		namespace detail
		{
			template <typename T>
			using is_reference = std::is_pointer<T>;

			template <typename T>
			using is_array = std::is_array<T>;
			
			template <typename> struct type_toString { static constexpr const char* value = "undefined"; };
			template <> struct type_toString<boolean> { static constexpr const char* value = "boolean"; };
			template <> struct type_toString<byte> { static constexpr const char* value = "byte"; };
			template <> struct type_toString<short> { static constexpr const char* value = "short"; };
			template <> struct type_toString<int> { static constexpr const char* value = "int"; };
			template <> struct type_toString<long> { static constexpr const char* value = "long"; };
			template <> struct type_toString<long long int> { static constexpr const char* value = "very long"; };
			template <> struct type_toString<float> { static constexpr const char* value = "float"; };
			template <> struct type_toString<double> { static constexpr const char* value = "double"; };
			template <> struct type_toString<long double> { static constexpr const char* value = "long double"; };
			template <> struct type_toString<char> { static constexpr const char* value = "char"; };
			template <> struct type_toString<java::lang::String> { static constexpr const char* value = "String"; };
		
			template <typename> struct package_toString { static constexpr const char* value = "undefined"; };
			template <> struct package_toString<boolean> { static constexpr const char* value = "java::lang::Boolean"; };
			template <> struct package_toString<byte> { static constexpr const char* value = "java::lang::Byte"; };
			template <> struct package_toString<short> { static constexpr const char* value = "java::lang::Short"; };
			template <> struct package_toString<int> { static constexpr const char* value = "java::lang::Integer"; };
			template <> struct package_toString<long> { static constexpr const char* value = "java::lang::Long"; };
			template <> struct package_toString<float> { static constexpr const char* value = "java::lang::Float"; };
			template <> struct package_toString<double> { static constexpr const char* value = "java::lang::Double"; };
			template <> struct package_toString<char> { static constexpr const char* value = "java::lang::Character"; };
		}

		struct Class 
		{
		protected:
			enum class Category {	
				instance,
				reference,
				array
			};

			template <typename T>
			struct resolve_category
			{
				static constexpr Category value = detail::is_reference<T>::value ? Category::reference 
			    									: (detail::is_array<T>::value ? Category::array 
    																			: Category::instance); 
			};

			template <Category cat>
			struct aliasOf
			{
				static constexpr const char* value = "undefined";
			};

			template <typename T>
			constexpr const char* categoryOf()
			{
				return aliasOf<resolve_category<T>::value>::value; 
			}

			template <typename T>
			struct typewrapper { using type = T; };
		public:
			Class() = default;

			template <typename T>
			Class(const typewrapper<T>&):
				typedesc({T::TYPENAME, categoryOf<T>(), sizeof(T)})
			{}

			template <template <typename> class T, typename U>
			Class(const typewrapper<T<U>>&):
				typedesc({T<U>::TYPENAME, categoryOf<T<U>>(), sizeof(T<U>), java::lang::detail::type_toString<U>::value})
			{}

			template <typename T>
			Class(const typewrapper<java::lang::IntegerOf<T>>&):
				typedesc({java::lang::detail::package_toString<T>::value, categoryOf<java::lang::IntegerOf<T>>(), sizeof(T)})
			{}

			template <typename T>
			Class(const typewrapper<java::lang::FloatingOf<T>>&):
				typedesc({java::lang::detail::package_toString<T>::value, categoryOf<java::lang::FloatingOf<T>>(), sizeof(T)})
			{}

			template <typename T>
			Class(const typewrapper<java::lang::CharacterOf<T>>&):
				typedesc({java::lang::detail::package_toString<T>::value, categoryOf<java::lang::CharacterOf<T>>(), sizeof(T)})
			{}

			~Class() = default;

			java::lang::String getName() const 
			{ 
				return java::lang::String(typedesc.alias) + 
						(typedesc.template_argument ? java::lang::String("<") + typedesc.template_argument + ">" 
													: "") + " #" + typedesc.category; 
			} 	
			
			long getSize() const { return typedesc.size; }

			template <typename T>
			static Class from()
			{
				return Class(typewrapper<T>{});
			}

		private:
			struct Typedesc {
				const char* alias;
				const char* category;
				std::size_t size;
				const char* template_argument = nullptr;
			} typedesc;
		};


		template <>
		struct Class::aliasOf<Class::Category::instance>
		{
			static constexpr const char* value = "obj";
		};

		template <>
		struct Class::aliasOf<Class::Category::reference>
		{
			static constexpr const char* value = "ref";
		};
		
		template <>
		struct Class::aliasOf<Class::Category::array>
		{
			static constexpr const char* value = "array";
		};


	} /// ns lang
} /// ns java


#if ! defined(TO_CSTRING)
# define STRINGIFY_HELPER(x) #x
# define TO_CSTRING(x) STRINGIFY_HELPER(x) 
#endif 

#if ! defined(java_object)
# define java_object_from(ns, alias, super) \
	friend class java::lang::Object; \
	public: \
		static constexpr const char* TYPENAME = TO_CSTRING(ns::alias); \
		\
		alias(const alias& other): super(other.pimpl) {} \
		\
		alias(alias&& other): super(std::move(other.pimpl)) {} \
		\
		alias(const java::lang::Object& obj) \
		{ \
			assign(obj); \
		} \
		\
		alias(java::lang::Object&& obj) \
		{ \
			assign(std::move(obj)); \
		} \
		\
	protected: \
		class Impl; \
		\
		alias(java::lang::Object::Impl* impl): super(impl) \
		{ if (!std::dynamic_pointer_cast<Impl>(std::shared_ptr<java::lang::Object::Impl>(impl))) { throw std::runtime_error("type error"); }} \
		alias(const std::shared_ptr<java::lang::Object::Impl>& impl): super(impl) \
		{ if (!std::dynamic_pointer_cast<Impl>(impl)) { throw std::runtime_error("type error"); }} \
		alias(std::shared_ptr<java::lang::Object::Impl>&& impl): super(std::move(impl)) \
		{ if (!std::dynamic_pointer_cast<Impl>(impl)) { throw std::runtime_error("type error"); }} \
		\
		void assign(const java::lang::Object& o) override \
		{ \
			if (dynamic_cast<const alias*>(&o)) { java::lang::Object::operator=(o); } \
			else throw std::runtime_error("type error"); \
		} \
		void assign(java::lang::Object&& o) override \
		{ \
			if (dynamic_cast<alias*>(&o)) { java::lang::Object::operator=(o); } \
			else throw std::runtime_error("type error"); \
		} \
	private: \
		inline Impl& getImpl() { return *std::dynamic_pointer_cast<Impl>(pimpl); } \
		inline const Impl& getImpl() const { return *std::dynamic_pointer_cast<const Impl>(pimpl); } 

# define java_object(ns, alias) java_object_from(ns, alias, java::lang::Object)
#endif

#if ! defined(java_object_impl)
# define java_object_impl(type) \
	public: \
		java::lang::Class getClass() const override { return java::lang::Class::from<type>(); } \
		java::lang::Object::Impl* clone() const override { return new type::Impl(*this); } \
	private:
#endif

#if ! defined(J_IMPL_GET)
#if ! defined(J_IMPL_POST)
# define J_IMPL_GET(method, ...) { return getImpl().method(__VA_ARGS__); }
# define J_IMPL_POST(method, ...) { getImpl().method(__VA_ARGS__); }
#endif
#endif