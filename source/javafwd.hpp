#pragma once

#include <type_traits>

/**
 * @typedef arithmetic primitives definitions 
 * ...most are the same in Java 
 */
using boolean = bool;
using byte = unsigned char;
// using short  	= short int;
// using int 		= int;
// using long 		= long int;
// using float 	= float;
// using double 	= double;
// using char 		= char;
// using void 		= void;


namespace java
{
	namespace lang
	{
		namespace detail
		{
			template <typename T> struct is_character : std::false_type {};
			template <> struct is_character<char> : std::true_type {};
			template <> struct is_character<char16_t> : std::true_type {};
			template <> struct is_character<char32_t> : std::true_type {};
			template <> struct is_character<wchar_t> : std::true_type {};
			template <template <typename> class T> struct is_character<T<char>> : std::true_type {};
			template <template <typename> class T> struct is_character<T<char16_t>> : std::true_type {};
			template <template <typename> class T> struct is_character<T<char32_t>> : std::true_type {};
			template <template <typename> class T> struct is_character<T<wchar_t>> : std::true_type {};
		}
		
		class Class;
		class Object;
		
		class System;
		class Thread;
		
		class Throwable;
		class Error;
		class Exception;

		class Number;
		template <typename T, template <typename> class TypeChecker>
		class PrimitiveOf;

		template <typename I>
		using IntegerOf = PrimitiveOf<I, std::is_integral>;
		
		template <typename F>
		using FloatingOf = PrimitiveOf<F, std::is_floating_point>;

		//! WCHAR, TCHAR, QCHAR AND ETC ARE NOT SUPPORTED YET
		template <typename C>
		using CharacterOf = PrimitiveOf<C, detail::is_character>; 

		class Math;
	}
}


/**
 * @typedef arithmetic primitives packages definitions
 */
namespace java
{
	namespace lang 
	{	
		using Boolean = IntegerOf<boolean>;
		using Byte = IntegerOf<byte>;
		using Short = IntegerOf<short int>;
		using Integer = IntegerOf<int>;
		using Long = IntegerOf<long int>;
		
		using Float = FloatingOf<float>;
		using Double = FloatingOf<double>;

		using Character = CharacterOf<char>;
		using String = std::string;
	}


	template <typename _Tp> 
	using ReferenceTo = _Tp*;
	using Reference = ReferenceTo<void>;
}


/**
 * 
 */
namespace java
{

	namespace detail
	{
		template <typename T, typename U, bool left>
		struct switch_helper
		{
			using type = U;	
		};

		template <typename T, typename U>
		struct switch_helper<T, U, true>
		{
			using type = T;
		};

		template <typename T, typename U>
		struct biggest_helper
		{
			static constexpr bool is_left() { return sizeof(T) > sizeof(U); }
		};

		template <typename T, typename U>
		struct biggest: switch_helper<T, U, biggest_helper<T, U>::is_left()> {};
	}

}

