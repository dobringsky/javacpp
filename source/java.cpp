#include <java/lang/Object>
#include <java/primitive/Integer>
#include <java/primitive/Floating>
#include <java/math.hpp>

size_t print(const boolean val)
{
	return fprintf(stdout, "boolean(%d)\n", val);
}

size_t print(const int val)
{
	return fprintf(stdout, "long(%d)\n", val);
}

size_t print(const long val)
{
	return fprintf(stdout, "long(%ld)\n", val);
}

size_t print(const double val)
{
	return fprintf(stdout, "double(%f)\n", val);
}

size_t print(const java::lang::String& val)
{
	return fprintf(stdout, "%s\n", val.c_str());
}

size_t printinfo(const java::lang::Object& obj)
{
	return fprintf(stdout, "objinfo: %s\n", obj.toString().c_str());
}


int main(int argc, char** argv) try
{
	if (argc != 1 && argv)
	{
		fprintf(stdout, "Silenced [-Wunused] warnings;\n\n");
	}

	java::lang::Object obj;
	java::lang::Number num;
	java::lang::Boolean boo = true; 

	java::lang::Short n25 = 25;
	java::lang::Integer n49 = 49;
	java::lang::Long n144 = 144;

	print(java::lang::String("Math::sqrt(25):"));
	print(java::lang::Math::sqrt(n25));
	print(java::lang::String("Math::sin(49):"));
	print(java::lang::Math::sin(n49)); 
	print(java::lang::String("Math::hypot(144, 49):"));
	print(java::lang::Math::hypot(n144, n49));

	printinfo(obj);
	printinfo(num);
	printinfo(boo);
	printinfo(n25);
	printinfo(n49);
	printinfo(n144);

	auto n49clone = n49.clone().as<java::lang::Integer>();
	printinfo(n49clone);

	java::lang::Double d = -2.03;

	print(d.toOctalString());
	print(d.toBinaryString());

	return 0;
}
catch(std::exception & ex)
{
	std::terminate();
	return -1;
}
